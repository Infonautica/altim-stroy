(function($, undefined) {
	var tabsTab = $('.tabs-top__tab');
	var tabsContent = $('.tabs-bottom__tab');

	tabsTab.on('click', function(event) {
		event.preventDefault();
		tabsTab.removeClass('tabs-top__tab--active');
		$(this).addClass('tabs-top__tab--active');
		var tabCurrent = $(this).data('tab');

		$('.tabs-bottom__tab--active').removeClass('tabs-bottom__tab--active');
		$('.tabs-bottom').find('.tabs-bottom__tab[data-tab="' + tabCurrent + '"]').addClass('tabs-bottom__tab--active');
	});
})(jQuery);