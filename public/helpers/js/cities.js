(function($, undefined) {
	$(".w-popup__select-container .options li, .tabs-top__select-container .options li").on('click', function() {
		var cityValue = $(this).attr("rel");
		$.ajax({
			method: "POST",
			url: '/city',
			data: JSON.stringify({ city: cityValue }),
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			success: function(res) {
				location.reload();
			},
			error: function() {
				alert("Ошибка! Пожалуйста, выберите город еще раз.");
			}
		});
	});
})(jQuery);