// Owl-carousel
(function($, undefined) {
	$(document).ready(function() {
		$("#carousel--created").owlCarousel({
			items: 4,
			itemsDesktop : false,
			itemsDesktopSmall : false,
			itemsTablet: false,
			itemsMobile: false,
			navigation: true
		});

		$("#carousel--last-objects").owlCarousel({
			items: 3,
			itemsDesktop : false,
			itemsDesktopSmall : false,
			itemsTablet: false,
			itemsMobile: false,
			navigation: true
		});
	});
})(jQuery);