function initialize() {
    var loc_0, map, marker, infobox;
    
    loc_0 = new google.maps.LatLng(55.75396, 37.620393);
    loc_1 = new google.maps.LatLng(35.75396, 30.620393);
    
    map = new google.maps.Map(document.getElementById("map__block"), {
         zoom: 12,
         center: loc_0,
         mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    for (var i = 0; i < 2; i++) {
	    marker = new google.maps.Marker({
	    	icon: '../img/elem/map__pin.png',
	        map: map,
	        position: loc_0,
	        visible: true
	    });
    };

    infobox = new InfoBox({
         content: document.getElementById("map__infobox"),
         disableAutoPan: false,
         maxWidth: 150,
         pixelOffset: new google.maps.Size(-140, 0),
         boxStyle: {
            background: "#007fca"
        },
        infoBoxClearance: new google.maps.Size(1, 1)
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        infobox.open(map, this);
        map.panTo(loc_0);
    });
}
google.maps.event.addDomListener(window, 'load', initialize);