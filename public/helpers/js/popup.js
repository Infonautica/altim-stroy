// Pop up window
$(document).ready(function() {
	// Popup opening
	$('.open-popup').click(function (e) {
		e.preventDefault();
		var $pop_up_f = ('#' + $(this).attr('data-win')),
			$popup = $($pop_up_f),
			$cur_height = $popup.height(),
			$cur_width = $popup.width(),
			$win_height = $(window).height(),
			$form = $popup.find('.w-form'),
			$form_id = $form.attr('id'),
			$link_id = $(this).attr('id'),
			$summary_id = ($form_id + '_' + $link_id);
		// $form.prop('id', $summary_id);
		$('.w-overlay').fadeIn();
		$($pop_up_f).show().css({'top': -$cur_height, 'margin-left': -($cur_width/2), 'margin-top': -($cur_height/2)}).animate({'top': ($win_height/2)});
	});
	// Popup closing
	$('.w-overlay, .w-popup__btn--continue, .w-popup__close, .w-popup__city-btn--agree').click(function(e){
		e.preventDefault();
		 if ($('.w-popup').is(':visible')) {
			$('.w-overlay, .w-popup').fadeOut('100');
		};
	});

	// Dropzone class:
	var ANY_FILES_ATTACHED = false;
	Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone("#myDropzone", 
	{
		// url: "/order",
		autoDiscover: false,
		addRemoveLinks: 'dictRemoveFile',
		paramName: "dropFile",
		autoProcessQueue: false,
		uploadMultiple: true,
		parallelUploads: 100,
		maxFiles: 100,
		previewsContainer: '.w-popup__dropzone',
		clickable: '.w-popup__dropzone'
	});

	myDropzone.on('addedfile', function() {
		console.log('addedfile');
		ANY_FILES_ATTACHED = true;
		$(".w-popup__dropzone").attr("data-content", "+ Прикрепить еще");
	});

	myDropzone.on('removedfile', function() {
		var fileCount = $(".w-popup__dropzone .dz-preview").length;
		if (fileCount <= 0) {
			$(".w-popup__dropzone").attr("data-content", "+ Прикрепить фото или файл сметы");
		}
	});

	myDropzone.on('success', function(file, res) {
		window.location.replace("/thanks/" + res.id);
	});



	/*
	 * Alert about whois choser
	 * @param form -- form element, which have whois field
	*/

	function alertWhois(form) {
		if (form.getElementsByClassName('w-popup__choser').length != 0) {
			form.getElementsByClassName('w-popup__you-are')[0].innerHTML = '<span class="w-popup__text--error">Укажите кем Вы являетесь:</span>';
		}
		return false;
	}



	/*
	 * Add whois field on forms
	 * @param form - form element, which one will include whois field
	*/

	function createWhois(form) {
		if (form.whois == undefined) {
			var whoisField = document.createElement('input');
			whoisField.setAttribute('type', 'hidden');
			whoisField.setAttribute('name', 'whois');
			form.appendChild(whoisField);
		}

		// Validate and get whois value
		var whoisElements = form.getElementsByClassName('js-choser__elem');
		for (var i = 0; i < whoisElements.length; i++) {
			whoisElements[i].addEventListener('click', function () {
				var content = this.innerText;
				this.offsetParent.querySelector('input[name="whois"]').value = content;
			});
		}
	}



	/*
	 * Validate if whois trigger checked or not
	 * @param form - form element, which have whois field
	*/

	function validateWhois(form) {
		if (form.getElementsByClassName('js-choser__elem--active').length === 0) {
			alertWhois(form);
			return false;
		} else {
			form.whois.value = form.getElementsByClassName('js-choser__elem--active')[0].innerText;
		}
	}



	/*
	 * Form controller class:
	 * @param event - click event
	 * @param this  - clicked input[type="submit"]
	*/

	function formController(event) {
		var currentForm;
		var isDropzone = Boolean(this.form.classList.contains('dropzone'));
		event.preventDefault();
		event.stopPropagation();

		if (isDropzone) {
			// Stop default submit controller
			currentForm = this.form;

			// Create whois field
			createWhois(currentForm);

			// Validate filled whois value
			validateWhois(currentForm);

			// Submitting dropzone form and files
			if (currentForm.getElementsByClassName('has-success').length == currentForm.querySelectorAll('input[data-validation="required"]').length) {
				if (ANY_FILES_ATTACHED) {
					myDropzone.processQueue();
				} else {
					currentForm.submit();
				}
			} else {
				alert('Не все заполнено');
			}
		} else {
			// If not dropzone popup
			currentForm = this.form;

			// Create whois field
			createWhois(currentForm);

			if (currentForm.getElementsByClassName('w-popup__choser').length != 0) {
				// Validate filled whois value
				validateWhois(currentForm)
			}

			if (currentForm.getElementsByClassName('has-success').length == currentForm.querySelectorAll('input[data-validation="required"]').length) {
				currentForm.submit();
			} else {
				alert('Не все заполнено');
				$.fn.validate();
			}
		}
	}

	// Set listeners on submits
	var popupSubmits = document.getElementsByClassName('w-popup__submit');
	for (var i = 0; i < popupSubmits.length; i++) {
		popupSubmits[i].addEventListener('click', formController);
	}
});