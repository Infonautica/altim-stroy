// Toggle comment box
(function($, undefined) {
	var commentToggleBtn = $(".order__comment-add");
	var commentToggleBtnText = {
		visible: "+ Добавить комментарий к заказу",
		hidden: "+ Скрыть комментарий к заказу"
	};
	var commentToggleClasses = "order__comment-block--hidden order__comment-block--visible";
	var commentBlock = $(".order__comment-block");

	commentToggleBtn.on('click', function(event) {
		// If hidden
		if (commentBlock.hasClass("order__comment-block--hidden")) {
			commentToggleBtn.text(commentToggleBtnText.hidden);
			commentBlock.toggleClass(commentToggleClasses);
		} else {
			commentToggleBtn.text(commentToggleBtnText.visible);
			commentBlock.toggleClass(commentToggleClasses);
		}
		event.preventDefault();
	});
})(jQuery);

// Cart deliting request
function removeRequest(id, cost) {
	$.ajax({
		method: "POST",
		url: '/cart/del',
		data: JSON.stringify({ id: id, cost: cost }),
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function(res) {
			var headerBasket = $(".header__basket");
			if (res.sum > 0) {
				headerBasket.removeClass("header__basket--empty header__basket--filled");
				headerBasket.addClass("header__basket--filled");
			} else {
				headerBasket.removeClass("header__basket--empty header__basket--filled");
				headerBasket.addClass("header__basket--empty");
			};
			$(".header__basket-sum").text(res.sum.toFixed());
			$(".cart__total-num--cost").text(res.sum.toFixed(2));
			$(".cart__total-num--pallets").text(res.pallets_count);
			$(".cart__total-num--weight").text(res.summary_weight);
			$(".cart__total-num--trucks").text(res.trucks_count);
		},
		error: function() {
			alert("Ошибка при удалении товара");
		}
	});
};

// Cart deleting
(function($, undefined) {
	$(".cart-table__buy-delete").on('click', function(event) {
		var self = $(this);
		var rowParent = self.parents('tr');
		$(this).parents("tr").fadeOut(200, function() {
			var prevRow = self.closest('tr').prev();
			var nextRow = self.closest('tr').next();
			
			var rowID = rowParent.data("id");
			var cost = rowParent.find('.cart-table__buy-price').text();
			
			// Send ajax-request and delete row from DOM
			removeRequest(rowID, cost);
			rowParent.remove();

			if (prevRow.hasClass("cart__table-section") && nextRow.hasClass("cart__table-section")) {
				prevRow.remove();
			} else if (prevRow.hasClass("cart__table-section") && nextRow.length == 0) {
				prevRow.remove();
			};
		});
		event.preventDefault();
	});
})(jQuery);