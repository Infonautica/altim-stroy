var locations = [
	['Тверь', 56.833206, 35.875532, '<p class="map__baloon-heading">Алтимстрой</p><p class="map__baloon-text">Офис продаж Тверь (Опт и Розница)</p><p class="map__baloon-text">Склад 1000 м2 Тверь</p><p class="map__baloon-text">г. Тверь, ул. Авангардная, 25а</p>', 'red'],
	['Воскресенск', 55.247457, 38.75581, '<p class="map__baloon-heading">Алтимстрой</p><p class="map__baloon-text">Склад 300 м2 - Московская область</p><p class="map__baloon-text">г.Воскресенск, ул. Гаражная, д.3</p>', 'red'],
	['Воскресенск', 55.296641, 38.709493, '<p class="map__baloon-heading">Волма-Воскресенск</p><p class="map__baloon-text">Прямая отгрузка с завода Волма для крупных заказов</p><p class="map__baloon-text">Склад готовой продукции 13500 м2</p><p class="map__baloon-text">Московская обл., Россия, 140200</p><p class="map__baloon-text">г.Воскресенск, ул. Кирова, 3, стр 1.</p>', 'blue'],
	['Минск', 53.903849, 27.593964, '<p class="map__baloon-heading">Волма-Минск</p><p class="map__baloon-text">Прямая отгрузка с завода Волма для крупных заказов</p><p class="map__baloon-text">г.Минск ул. Козлова 24, Беларусь</p>', 'blue'],
	['Москва', 55.718441, 37.606873, '<p class="map__baloon-heading">Торговый дом Алтимстрой</p><p class="map__baloon-text">Офис продаж Москва (только Опт)</p><p class="map__baloon-text">ул. Шаболовка, 34, Москва, Россия</p><p class="map__baloon-text">Тел.: +7 495 795-39-60</p>', 'red']
];

(function initialize() {
	var myOptions = {
		center: new google.maps.LatLng(55.4, 33.920393),
		scrollwheel: false,
		mapTypeControl: false,
		navigationControl: false,
		scaleControl: true,
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map__block"), myOptions);
	setMarkers(map,locations);
	map.setCenter(myOptions.center);
})();



function setMarkers(map,locations){
	var marker, i

	for (i = 0; i < locations.length; i++) {  
		var title = locations[i][0];
		var lat = locations[i][1];
		var long = locations[i][2];
		var baloon =  locations[i][3];
		var pinColor =  locations[i][4];
		var pinImageUrl;

		if (pinColor == 'blue') {
			pinImageUrl = '../../img/elem/map__pin--blue.png';
		} else {
			pinImageUrl = '../../img/elem/map__pin--red.png';
		}

		latlngset = new google.maps.LatLng(lat, long);
		var marker = new google.maps.Marker({  
			map: map,
			title: title ,
			position: latlngset,
			icon: pinImageUrl 
		});
		map.setCenter(marker.getPosition())

		var content = '<div class="map__baloon">' + baloon + '</div>';
		var infowindow = new google.maps.InfoWindow({
			maxWidth: 400
		});

		google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
			return function() {
				infowindow.setContent(content);
				infowindow.open(map,marker);
			};
		})(marker,content,infowindow)); 
	}
}