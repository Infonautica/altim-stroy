(function($, undefined) {
	$(".spec-table__buy-btn").on('click', function() {
		var currentRow = $(this).parents('tr');

		var itemParameters = {
			id: currentRow.data('id'),
			name: currentRow.find('td[data-item="name"]').text(), // none
			// sizes: currentRow.find('td[data-item="sizes"]').text(),
			// consumption: currentRow.find('td[data-item="consumption"]').text(),
			weight: currentRow.find('td[data-item="weight"]').text(), // none
			pallet_count_pcs: currentRow.find('td[data-item="pallet_count_pcs"]').text(),
			// pallet_count_ms: currentRow.find('td[data-item="pallet_count_ms"]').text(),
			truck_pallets_count: currentRow.find('td[data-item="truck_pallets_count"]').text(), // none
			truck_pcs_count: currentRow.find('td[data-item="truck_pcs_count"]').text(), // none
			truck_ms_count: currentRow.find('td[data-item="truck_ms_count"]').text(), // none
			truck_kg_count: currentRow.find('td[data-item="truck_kg_count"]').text(), // none
			pcs_cost: currentRow.find('td[data-item="pcs_cost"]').text(),
			pallets_truck_cost: currentRow.find('td[data-item="pallets_truck_cost"]').text(), // none
			total_count: currentRow.find('td[data-item="total_count"]').find(".spec-table__buy-count").val()
		};

		$.ajax({
			method: "POST",
			url: '/cart/add',
			data: JSON.stringify(itemParameters),
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			success: function(res) {
				$("#popup--cart").find(".w-popup__heading").find("span#pallets").text(res.pallets_count);
				$("#popup--cart").find(".w-popup__heading").find("span#cost").text(res.pallets_cost.toFixed(2));
				if (res.sum > 0) {
					$(".header__basket").removeClass("header__basket--empty header__basket--filled");
					$(".header__basket").addClass("header__basket--filled");
				} else {
					$(".header__basket").removeClass("header__basket--empty header__basket--filled");
					$(".header__basket").addClass("header__basket--empty");
				};
				$(".header__basket-sum").text(res.sum.toFixed());
			},
			error: function(error) {
				alert("Ошибка при добавлении товара в корзину");
				console.log(error);
			}
		});
	});
})(jQuery);