// console.log("Description: " + value);
function recalculateCart() {
	// Объекты, отображающие информацию
	var total_values = {
		pallets: document.getElementsByClassName('cart__total-num--pallets')[0],
		weight: document.getElementsByClassName('cart__total-num--weight')[0],
		trucks: document.getElementsByClassName('cart__total-num--trucks')[0],
		price: document.getElementsByClassName('cart__total-num--cost')[0],
		header: document.getElementsByClassName('header__basket-sum')[0]
	};

	// Переменные итого:
	var global_values = {
		pallets: 0,
		weight: 0,
		price: 0
	};

	// Строки таблицы
	var priceRow = document.getElementsByClassName('cart-table__row');

	// Цикл по всем строкам в таблице
	for (var i = 0; i < priceRow.length; i++) {
		// Значения строки таблицы
		var local_values = {
			pallets: 0,
			weight: 0,
			price: 0
		};

		// Заполняем значения
		local_values.pallets = priceRow[i].getElementsByClassName('cart-table__buy-count')[0].value; // Кол-во поддонов в товаре

		// Сразу же обновляем цену в строке для дальнейших рассчетов
		var pallet_count_pcs = parseInt(priceRow[i].getElementsByClassName('table-cell--center')[1].innerHTML) / parseInt(priceRow[i].getElementsByClassName('table-cell--center')[0].innerHTML); // Кол-во штук на поддоне
		priceRow[i].getElementsByClassName('cart-table__buy-price')[0].innerHTML = ((local_values.pallets * pallet_count_pcs * parseFloat(priceRow[i].getElementsByClassName('table-cell--center')[4].innerHTML)).toFixed(2) + " ₽").replace(/\B(?=(\d{3})+(?!\d))/g, " ");

		local_values.weight = local_values.pallets * (priceRow[i].getElementsByClassName('table-cell--center')[3].innerHTML / priceRow[i].getElementsByClassName('table-cell--center')[0].innerHTML); // Вес всех поддонов
		local_values.price = priceRow[i].getElementsByClassName('cart-table__buy-price')[0].innerHTML;


		// Добавляем в общий подсчет
		global_values.pallets += parseInt(local_values.pallets);
		global_values.weight += parseInt(local_values.weight);
		global_values.price += parseFloat(local_values.price.replace(/\s+/g, ''));
	}

	// Выводим информацию
	total_values.pallets.innerHTML = global_values.pallets;
	total_values.weight.innerHTML = global_values.weight + " кг";
	total_values.trucks.innerHTML = Math.ceil(global_values.weight/20000);
	total_values.price.innerHTML = global_values.price.toFixed(2) + " ₽";
	total_values.header.innerHTML = Math.floor(global_values.price).toFixed();
}

// Добавляем слушателей на плюсы и минусы
var recalculateTriggers = document.getElementsByClassName('cart-table__buy-trigger');
for (var i = 0; i < recalculateTriggers.length; i++) {
	recalculateTriggers[i].addEventListener('click', recalculateCart);
}

var recalculateFields = document.getElementsByClassName('cart-table__buy-count');
for (var i = 0; i < recalculateFields.length; i++) {
	recalculateFields[i].addEventListener('input', recalculateCart);
}

// Вешаем основной обработчик на кнопку
var goNext = document.getElementsByClassName('cart__next-btn')[0];
goNext.addEventListener('click', function(e) {
	e.preventDefault();
	// Собираем массив объектов из таблицы
	var cartRow = document.getElementsByClassName('cart-table__row');
	var cartArray = [];
	for (var i = cartRow.length - 1; i >= 0; i--) {
		var cartObject = {
			id: "",
			pallets_count: 0
		};

		cartObject.id = cartRow[i].getAttribute("data-id");
		cartObject.pallets_count = cartRow[i].getElementsByClassName('cart-table__buy-count')[0].value;
		cartArray.push(cartObject);
	}
	$.ajax({
		method: "POST",
		url: '/cart/cpc',
		data: JSON.stringify({ cpcObj: cartArray }),
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function (res) {
			window.location.replace('/order');
		}
	});
});