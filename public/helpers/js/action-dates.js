(function($, undefined) {
	var now = new Date();
	var actionDate = new Date();

	actionDate.setDate(now.getDate() + 14);
	actionDay = actionDate.getDate();
	actionMonth = actionDate.getMonth();

	if (actionMonth == 0) { monthName = 'января' }
	if (actionMonth == 1) { monthName = 'февраля' }
	if (actionMonth == 2) { monthName = 'марта' }
	if (actionMonth == 3) { monthName = 'апреля' }
	if (actionMonth == 4) { monthName = 'мая' }
	if (actionMonth == 5) { monthName = 'июня' }
	if (actionMonth == 6) { monthName = 'июля' }
	if (actionMonth == 7) { monthName = 'августа' }
	if (actionMonth == 8) { monthName = 'сентября' }
	if (actionMonth == 9) { monthName = 'октября' }
	if (actionMonth == 10) { monthName = 'ноября' }
	if (actionMonth == 11) { monthName = 'декабря' }

	$('.js-action__day').text(actionDay);
	$('.js-action__month').text(monthName);
})(jQuery);