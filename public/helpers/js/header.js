$(document).ready(function(){
    $('a.header__navigation-link, .footer__list-link').bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });
    return false;
});

(function() {
    var lastScrollTop = 0;
    $(window).scroll(function(){
        var st = $(this).scrollTop();

        if (st > 500) {
	        if (st < lastScrollTop || st === 0){
	            $('#s-header').removeClass('header--hidden');
	        } else {
	            $('#s-header').addClass('header--hidden');
	        };
	        lastScrollTop = st;
        };
    });
})(jQuery);