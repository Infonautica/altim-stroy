var menu = $('#s-navigation'),
	menuImg = $('.navbar-brand img'),
	showPoint = $('#s-promo').height();

var minifyMenu = function() {
	menu.addClass('fadeOut'); // 1. Hide menu
	menuImg.attr('src', 'img/elements/home.png'); // 2. Make chngs
	menu.addClass('mini');
	menu.removeClass('fadeOut');
	menu.addClass('fadeIn'); //3. Show menu
}

var maxifyMenu = function() {
	menu.addClass('fadeOut');
	menuImg.attr('src', 'img/elements/logo.png');
	menu.removeClass('mini').removeClass('fadeOut');
	// menu.addClass('fadeIn'); //3. Show menu
}

var clear = function() {
	menu.removeClass('fadeOut');
	menu.removeClass('fadeIn');
}

$(function() {
    $(window).scroll(function() {
        var top = $(document).scrollTop();
        if (top > showPoint) {
        	clear();
			minifyMenu();
		} else {
			clear();
			maxifyMenu();
		}
    });
});