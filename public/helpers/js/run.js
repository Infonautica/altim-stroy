(function($, undefined) {
	$(document).ready(function() {

		// Маска телефонов
		$('input[type="tel"]').mask('+7 (999) 999-99-99');

		// Валидация форм
		$.validate({
			validateOnBlur : true,
			showHelpOnFocus : false,
			addSuggestions : false,
			scrollToTopOnError : false,
			borderColorOnError : '#d71818',
			form: '.validate',
			onSuccess : function(form) {
				/*if (validateYouAre(form) == false) {
					return false;
				} else {
					var m_data = form.serialize();
					var currentForm = $(eval($(form).context));
					var youAreValue = currentForm.find(".js-choser__elem--active").text();
					m_data += "&whois=" + youAreValue.replace(/\s/g, '+');
					console.log(m_data);
					$.ajax({
						method: "POST",
						url: '/order',
						data: m_data,
						success: function(res) {
							window.location.replace("/thanks/" + res.id);
						}
					});
				}
				return false;*/
			}
		});

		// Lightbox
		$(".lightbox").lightGallery({
			selector: '.lightbox__link'
		});
		$(".lightgallery--video").lightGallery({
			videoMaxWidth: "900px",
			controls: false
		});

		// Popup "Your city"
		var cityPopup = document.getElementById('popup--city');
		var pageName = cityPopup.getAttribute('data-page');
		var isShopPage = false;
		if (pageName != 'home' && pageName != 'cart' && pageName != 'order') {
			isShopPage = true;
		}
		var city_final = localStorage.getItem('isSaved');
		var asked = false;

		function runCityAsk() {
			if (city_final) {
				return false;
			} else {
				localStorage.setItem('isSaved', 'true');
				var $pop_up_f = ('#popup--city'),
					$popup = $($pop_up_f),
					$cur_height = $popup.height(),
					$cur_width = $popup.width(),
					$win_height = $(window).height();
				$('.w-overlay').fadeIn();
				$($pop_up_f).show().css({'top': -$cur_height, 'margin-left': -($cur_width/2), 'margin-top': -($cur_height/2)}).animate({'top': ($win_height/2)});
			}

			cityPopup.getElementsByClassName('w-popup__city-btn--disagree')[0].addEventListener('click', function(event) {
				cityPopup.getElementsByClassName('w-popup__city-btns')[0].classList.toggle('w-popup__city-btns--active');
				cityPopup.getElementsByClassName('w-popup__city-btns')[0].classList.toggle('w-popup__city-btns--hidden');
				cityPopup.getElementsByClassName('w-popup__city-select')[0].classList.toggle('w-popup__city-select--active');
				cityPopup.getElementsByClassName('w-popup__city-select')[0].classList.toggle('w-popup__city-select--hidden');
				event.preventDefault();
			});
		}

		if (isShopPage == true) {
			$(window).scroll(function() {
				if ($(this).scrollTop() > 500 && asked == false) {
					runCityAsk();
					asked = true;
				}
			});
		}
	});
})(jQuery);