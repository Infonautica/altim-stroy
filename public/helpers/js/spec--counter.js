// Счетчик кол-ва товара
(function($, undefined) {
	$('.js_counter__plus').on('click', function(event) {
		var ourCounter = $(this).parents('.js_counter');
		var ourCount = ourCounter.find('.js_counter__count');
		var ourValue;
		if (parseInt(ourCount.val(), 10) >= 0 && parseInt(ourCount.val(), 10) <= 998) {
			ourValue = parseInt(ourCount.val(), 10) + 1;
		};
		ourCount.val(ourValue);
		event.preventDefault();
	});

	$('.js_counter__minus').on('click', function(event) {
		var ourCounter = $(this).parents('.js_counter');
		var ourCount = ourCounter.find('.js_counter__count');
		var ourValue;
		if (parseInt(ourCount.val(), 10) > 0 && parseInt(ourCount.val(), 10) < 999) {
			ourValue = parseInt(ourCount.val(), 10) - 1;
		};
		ourCount.val(ourValue);
		event.preventDefault();
	});
})(jQuery);