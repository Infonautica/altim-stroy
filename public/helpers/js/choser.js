// Choser
(function($, undefined) {
	var jsChoser = $(".js-choser");
	var jsChoser__elem = jsChoser.find(".js-choser__elem");
	jsChoser__elem.on('click', function(event) {
		jsChoser.find(".js-choser__elem--active").removeClass("js-choser__elem--active");
		$(this).toggleClass("js-choser__elem--active");
		event.preventDefault();
	});
})(jQuery);